package parser

import "testing"

// TestParse1 asserts the string input
// {"0 292 399 307"} is correctly turned into
// a 2D int array representing barren land
// rectangles
func TestParse1(t *testing.T) {
	input := "{\"0 292 399 307\"}"
	expected := [][]int{{0, 292, 399, 307}}

	output, err := ParseBarrenLandSet(input)
	if err != nil {
		t.Fatalf("ParseBarrenLandSet(%v) failed with error: %v", input, err)
	} else if len(output) != len(expected) || len(output[0]) != len(expected[0]) {
		t.Fatalf("ParseBarrenLandSet(%v) expected %v got %v", input, expected, output)
	} else {
		match := true

		for i := range expected {
			for j := range expected[i] {
				if expected[i][j] != output[i][j] {
					match = false
					break
				}
			}
		}

		if !match {
			t.Fatalf("ParseBarrenLandSet(%v) expected %v got %v", input, expected, output)
		}
	}
}

// TestParse1 asserts the string input
// {"48 192 351 207", "48 392 351 407", "120 52 135 547", "260 52 275 547"}
// is correctly turned into a 2D int array
// representing barren land rectangles
func TestParse2(t *testing.T) {
	input := "{\"48 192 351 207\", \"48 392 351 407\", \"120 52 135 547\", \"260 52 275 547\"}"
	expected := [][]int{{48, 192, 351, 207}, {48, 392, 351, 407}, {120, 52, 135, 547}, {260, 52, 275, 547}}

	output, err := ParseBarrenLandSet(input)
	if err != nil {
		t.Fatalf("ParseBarrenLandSet(%v) failed with error: %v", input, err)
	} else if len(output) != len(expected) || len(output[0]) != len(expected[0]) {
		t.Fatalf("ParseBarrenLandSet(%v) expected %v got %v", input, expected, output)
	} else {
		match := true

		for i := range expected {
			for j := range expected[i] {
				if expected[i][j] != output[i][j] {
					match = false
					break
				}
			}
		}

		if !match {
			t.Fatalf("ParseBarrenLandSet(%v) expected %v got %v", input, expected, output)
		}
	}
}
