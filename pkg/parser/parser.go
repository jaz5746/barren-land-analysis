package parser

import (
	"encoding/json"
	"errors"
	"strconv"
	"strings"
)

// ParseBarrenLandSet takes a string of form
// `{"48 192 351 207", "48 392 351 407"}` that represents
// a list of rectangles that represent barren land markings
// and returns the list formatted into a 2D int array
func ParseBarrenLandSet(input string) ([][]int, error) {
	var rectangles []string
	// Replace {} with [] to create valid json
	in := strings.Replace(strings.Replace(input, "{", "[", 1), "}", "]", 1)

	err := json.Unmarshal([]byte(in), &rectangles)
	if err != nil {
		return nil, err
	}

	barrenLand := make([][]int, len(rectangles))

	// Convert the string array values into ints
	for i, rect := range rectangles {
		coords := strings.Split(rect, " ")

		barrenLand[i] = make([]int, len(coords))
		for j, coord := range coords {
			var err error
			barrenLand[i][j], err = strconv.Atoi(coord)
			if err != nil {
				return barrenLand, errors.New("Could not convert input to integers")

			}
		}
	}
	return barrenLand, nil
}
