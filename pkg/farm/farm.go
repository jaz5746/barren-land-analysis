package farm

import (
	"bytes"
	"errors"
	"fmt"
	"sort"
)

// A Farm is represented as a 2D array of bools.
// If a bool is false in the unit of land, then
// the land is not barren and the land can be farmed
// upon. If the bool is true, then the land is
// barren and cannot be farmed upon
type Farm struct {
	Land [][]bool
}

// NewFarm takes barrenLand as coordinates for rectangles
// in the land to be marked as "barren," and returns
// pointer to a Farm struct and any errors that occur
// if the farm land is invalid
func NewFarm(m int, n int, barrenLand [][]int) (*Farm, error) {
	err := validateBarrenLand(m, n, barrenLand)
	if err != nil {
		return nil, err
	}

	if m < 1 || n < 1 {
		return nil, errors.New("Values M and N must be greater than 0")
	}

	p := new(Farm)

	land := make([][]bool, m)
	for i := range land {
		land[i] = make([]bool, n)
	}

	for _, rect := range barrenLand {
		xStart := rect[0]
		xEnd := rect[2]
		yStart := rect[1]
		yEnd := rect[3]
		for i := xStart; i <= xEnd; i++ {
			for j := yStart; j <= yEnd; j++ {
				land[i][j] = true
			}
		}
	}
	p.Land = land

	return p, nil
}

// validateBarrenLand validates that all coordinates
// provided in `land` are rectangles with 4 coordinates
// within the boundaries provided by `m` and `n`
// where `m` is the length and `n` is the width of the land
// returns an error with a description of the problem
// if found and nil otherwise
func validateBarrenLand(m int, n int, barrenLand [][]int) error {
	for _, rect := range barrenLand {
		if len(rect) != 4 {
			return errors.New("Barren Land rectangles require 4 coordinates")
		}

		for i, coord := range rect {
			if coord < 0 || (i < 2 && coord >= m) || (i >= 2 && coord >= n) {
				return fmt.Errorf("Barren Land coordinate %d found outside of boundaries", coord)
			}
		}

		if rect[2] < rect[0] || rect[3] < rect[1] {
			return fmt.Errorf("Barren Land coordinates %v must have the first two integers be the coordinates of "+
				"the bottom left corner in the given rectangle, and the last two integers be the coordinates of the top right corner", rect)
		}
	}

	return nil
}

// GetFarmPlotSizes returns an array of the sizes
// of every continuous non-barren land and any errors
// that occur
func (farm Farm) GetFarmPlotSizes() ([]int, error) {
	land := farm.Land
	queueX := make([]int, 0)
	queueY := make([]int, 0)
	openPlots := make([]int, 0)

	// Create a seen matrix that replicates the land
	// All values default to false
	seen := make([][]bool, len(land))
	for i := range seen {
		seen[i] = make([]bool, len(land[i]))
	}

	for i, row := range land {
		for j, node := range row {
			// If we haven't seen this node, and this is not barren land, add to queue
			if !seen[i][j] && !node {
				queueX = append(queueX, i)
				queueY = append(queueY, j)
				seen[i][j] = true
			}
			count := 0
			for len(queueX) > 0 {
				// Pop off the top coords from the queue
				x := queueX[0]
				y := queueY[0]
				queueX = queueX[1:]
				queueY = queueY[1:]

				count++

				// add neighbors to queue
				for k := -1; k <= 1; k++ {
					// check for out of bounds
					if x+k < 0 || x+k >= len(land) {
						continue
					}
					for v := -1; v <= 1; v++ {
						// check for out of bounds, if seen, and if barren land
						if (y+v < 0 || y+v >= len(land[0])) || seen[x+k][y+v] || land[x+k][y+v] {
							continue
						} else {
							queueX = append(queueX, x+k)
							queueY = append(queueY, y+v)
							// mark as seen as we add to queue so we don't infinitely loop
							seen[x+k][y+v] = true
						}
					}
				}
			}
			if count > 0 {
				openPlots = append(openPlots, count)
			}
		}
	}

	// Sort values
	sort.Slice(openPlots, func(i, j int) bool {
		return openPlots[i] < openPlots[j]
	})

	return openPlots, nil
}

func (farm Farm) String() string {
	var buffer bytes.Buffer
	for i := len(farm.Land) - 1; i >= 0; i-- {
		for _, j := range farm.Land[i] {
			if j {
				buffer.WriteString("1")
			} else {
				buffer.WriteString("0")
			}
		}
		if i > 0 {
			buffer.WriteString("\n")
		}
	}
	return buffer.String()
}
