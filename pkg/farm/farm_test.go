package farm

import "testing"

// TestFarm1 creates a Farm struct of size 400x600 with
// barren land plots denoted by {{0, 292, 399, 307}} and
// tests that the return values are correct
func TestFarm1(t *testing.T) {
	M := 400
	N := 600
	barrenLand := [][]int{{0, 292, 399, 307}}

	farm, err := NewFarm(M, N, barrenLand)
	if err != nil {
		t.Fatalf("NewFarm(%d, %d, %v) failed with error %v", M, N, barrenLand, err)
	}

	expected := []int{116800, 116800}

	plots, err := farm.GetFarmPlotSizes()
	if err != nil {
		t.Fatalf("GetFarmPlotSizes() failed with error: %v", err)
	} else if len(plots) != len(expected) || (len(plots) == len(expected) && (plots[0] != expected[0] || plots[1] != expected[1])) {
		t.Fatalf("GetFarmPlotSizes() with %v expected %v got %v", barrenLand, expected, plots)
	}
}

// TestFarm2 creates a Farm struct of size 400x600 with
// barren land plots denoted by
// {{48, 192, 351, 207}, {48, 392, 351, 407}, {120, 52, 135, 547}, {260, 52, 275, 547}}
// and tests that the return values are correct
func TestFarm2(t *testing.T) {
	M := 400
	N := 600
	barrenLand := [][]int{{48, 192, 351, 207}, {48, 392, 351, 407}, {120, 52, 135, 547}, {260, 52, 275, 547}}

	farm, err := NewFarm(M, N, barrenLand)
	if err != nil {
		t.Fatalf("NewFarm(%d, %d, %v) failed with error %v", M, N, barrenLand, err)
	}

	expected := []int{22816, 192608}

	plots, err := farm.GetFarmPlotSizes()
	if err != nil {
		t.Fatalf("GetFarmPlotSizes() failed with error: %v", err)
	} else if len(plots) != len(expected) || (len(plots) == len(expected) && (plots[0] != expected[0] || plots[1] != expected[1])) {
		t.Fatalf("GetFarmPlotSizes() with %v expected %v got %v", barrenLand, expected, plots)
	}
}
