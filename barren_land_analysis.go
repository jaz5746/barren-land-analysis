package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"

	"gitlab.com/jaz5746/barren-land-analysis/pkg/farm"
	parser "gitlab.com/jaz5746/barren-land-analysis/pkg/parser"
)

func main() {
	var M int
	var N int
	var visual bool
	var help bool
	flag.IntVar(&M, "m", 400, "Length of land to analyze - positive integer")
	flag.IntVar(&N, "n", 600, "Width of land to analyze - positive integer")
	flag.BoolVar(&visual, "visual", false, "Whether or not to print out a visual of the land")
	flag.BoolVar(&help, "help", false, "Prints this usage message")
	flag.Parse()

	if help {
		fmt.Println("Usage: barren_land_analysis.go [options]")
		flag.PrintDefaults()
		os.Exit(0)
	}

	if M < 1 || N < 1 {
		fmt.Println("Usage: barren_land_analysis.go -m M -n N")
		fmt.Println("Values M and N must be greater than 0")
		flag.PrintDefaults()
		os.Exit(1)
	}

	// Get input
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	in := scanner.Text()

	// Parse input
	barrenLand, err := parser.ParseBarrenLandSet(in)
	if err != nil {
		fmt.Println("Error reading input")
		panic(err)
	}

	// Create a Farm
	farm, err := farm.NewFarm(M, N, barrenLand)
	if err != nil {
		panic(err)
	}

	if visual {
		fmt.Println(farm)
	}

	openPlots, err := farm.GetFarmPlotSizes()
	if err != nil {
		panic(err)
	}

	if len(openPlots) > 0 {
		for _, v := range openPlots {
			fmt.Print(v, " ")
		}
		fmt.Println()
	} else {
		fmt.Println(0)
	}
}
