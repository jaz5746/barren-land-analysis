# Barren Land Analysis

## Case Study Requirements

You have a farm of 400m by 600m where coordinates of the field are from (0, 0)
to (399, 599).A portion of the farm is barren, and all the barren land is in
the form of rectangles. Due to these rectangles of barren land, the remaining
area of fertile land is in no particular shape. An area of fertile land is
defined as the largest area of land that is not covered by any of the
rectangles of barren land.

Read input from STDIN. Print output to STDOUT

### Input

You are given a set of rectangles that contain the barren land. These
rectangles are defined in a string, which consists of four integers
separated by single spaces, with no additional spaces in the string. The
first two integers are the coordinates of the bottom left corner in the
given rectangle, and the last two integers are the coordinates of the top
right corner.

### Output

Output all the fertile land area in square meters, sorted from smallest
area to greatest, separated by a space.

### Examples

| Sample Input | Sample Output |
| ------------ | ------------- |
| {"0 292 399 307"} | 116800  116800 |
| {"48 192 351 207", "48 392 351 407", "120 52 135 547", "260 52 275 547"} | 22816 192608 |

## Technology Stack

This project was developed using Go version 1.8.3 on Linux.

## Build and Run

On your own system within this repository, run the following to compile a
binary to run this project.

```console
go build
```

This should output a `barren-land-analysis` binary file which you can then run
to start the program on your command line. You can run `go help build` for more
options on how go can build the project.

Alternatively, you can run the following, which will run the project without
building a separate binary.

```console
go run barren_land_analysis.go
```

There are also a few configurable variables that you can check out by appending
the `-help` flag to the command.

```console
$ go run barren_land_analysis.go -help
Usage: barren_land_analysis.go [options]
  -help
        Prints this usage message
  -m int
        Length of land to analyze - positive integer (default 400)
  -n int
        Width of land to analyze - positive integer (default 600)
  -visual
        Whether or not to print out a visual of the land
```

## Solution

### The Idea

Instead of a 2D array as the plot of land, imagine the plot of land as a
set of disconnected graphs that are undirected and cyclical, where each unit
of non-barren land is a vertex, and there are edges between all of its
non-barren neighbors. From here, we can do a breadth first traversal (BFT),
attempting to start on each unit of land, but marking them as seen as we add
them to the queue to avoid repeats. Every time we start a new BFT, a new plot
is found and we record the size of that count each time we pop a node off the
queue. Sort the recorded sizes of each plot at the end and we're done!

### The Breadth First Traversal

1. Create a 2D array of size (M, N), (400, 600) for this case study, where
    all entries are marked `false` initially
2. For each barren plot of land, mark points within the 2D array as `true`
3. For each unit of land that is not marked as `seen` and is non-barren
    (false in our land array)
    * Mark the unit of land as `seen` and add it to the Queue
    * While the Queue is not empty
        * Pop the top unit of land off of the Queue
        * Increment `count` by one
        * Iterate through all of this unit of land's neighbors
            * Add neighbor to the Queue if it has not been seen and it is not
                barren land
            * Mark neighbor as `seen` if it has not been already
    * Save `count` to a list and reset it to 0
4. Sort and return all non-zero `count` values that were saved

### Alternative Solution

The above solution is rather brute force as it iterates over the entire land to
count each unit of non-barren land individually. I can imagine that there is
another solution that can identify where all of the barren land plots intersect
that create disconnected plots of non-barren land, and then do math to
calculate each of those plot sizes, as well as the outer plot of land.

Given the constraints on this case study that the land will always be 400 by
600, which can be brute forced quickly enough, I determined that this solution
would be acceptable, which allowed me to spend the time I would otherwise have
spent coming up with a different solution working on other parts of this
project, like the pipeline, parameterizing values, writings tests, and writing
strong documentation.

## Pipeline

As seen in the [gitlab-ci](.gitlab-ci.yml) file, this project has a
pipeline built out for testing that runs every time a new
commit is made, or if one is triggered manually in gitlab. The pipeline
asserts linting is done on the project using `golint`, that the project
can build, runs a few test cases, and runs all of the go tests in
the project.

There is also an option when you manually run the pipeline to pass
in environment variables to run the project with a custom set of
parameters and custom input.

| Variable | Description | Default Value |
| -------- | ----------- | ------------- |
| CUSTOM_RUN | Whether or not to run | false |
| M | Length of land to analyze - positive integer | 400 |
| N | Width of land to analyze - positive integer | 600 |
| VISUAL | Whether or not to print out a visual of the land | false |
| INPUT | The input to give the project through stdin | {} |

Setting the `CUSTOM_RUN` parameter to `true` will trigger an extra job to run
in the `test` phase of the pipeline that will run with the given parameters.

## Testing and Results

The project is to be run on the command line, and will accept input from stdin.
There are test inputs within the [test-inputs](./test-inputs/) directory that
one can use to feed the input on the command line. Below is the output of
running the project with the different inputs within the
[test-inputs](./test-inputs/) directory. Note that when the `-visual` flag is
added, a grid is printed out of 0s and 1s, where 0 represents non-barren land
and 1s represent barren land.

### Sample Case 1

```console
$ go run barren_land_analysis.go
{"0 292 399 307"}
116800 116800
```

### Sample Case 2

```console
$ go run barren_land_analysis.go
{"48 192 351 207", "48 392 351 407", "120 52 135 547", "260 52 275 547"}
22816 192608
```

### Case 3

```console
$ go run barren_land_analysis.go -m 30 -n 30 -visual
{"0 10 29 14"}
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
000000000011111000000000000000
300 450
```

### Case 4

```console
$ go run barren_land_analysis.go -m 30 -n 30 -visual < test-inputs/test-input-4.txt
000000000000000000000000000000
000000000000000000000000000000
000000000000000000000000000000
000000000000000000000000000000
000001000011000000000000010000
000001000011000000000000010000
000111111111111111111111111100
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000111111111111111111111111100
000001000011000000000000010000
000001000011000000000000010000
000001000011000000000000010000
000000000000000000000000000000
000000000000000000000000000000
000000000000000000000000000000
000000000000000000000000000000
000000000000000000000000000000
56 182 536
```

### Go Tests

A package for [`farm`](./pkg/farm/) and [`parser`](./pkg/parser/) were
created for this project, and they have tests for their functionality
to assert they output the correct results for a few test cases. To run
these tests for all packages, run the following:

```console
go test ./... -v
```
